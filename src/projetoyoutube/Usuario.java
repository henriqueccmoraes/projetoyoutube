/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package projetoyoutube;

/**
 *
 * @author user
 */
public class Usuario extends Dados {
    
    private String login;
    private int quantAssistido;

    public Usuario(String login, String nome, int idade, String sexo) {
        super(nome, idade, sexo);
        this.login = login;
        this.quantAssistido = 0;
    }
    
    

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getQuantAssistido() {
        return quantAssistido;
    }

    public void setQuantAssistido(int quantAssistido) {
        this.quantAssistido = quantAssistido;
    }

    @Override
    public String toString() {
        return "Usuario{" + super.toString() + "login=" + login + ", quantAssistido=" + quantAssistido + '}';
    }
    
    
    

    }
