/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package projetoyoutube;

/**
 *
 * @author user
 */
public class Visualizacao {
    
    private Usuario aluno;
    private Video projeto;

    public Visualizacao(Usuario aluno, Video projeto) {
        this.aluno = aluno;
        this.projeto = projeto;
    }

    public Usuario getAluno() {
        return aluno;
    }

    public void setAluno(Usuario aluno) {
        this.aluno = aluno;
    }

    public Video getProjeto() {
        return projeto;
    }

    public void setProjeto(Video projeto) {
        this.projeto = projeto;
    }

    @Override
    public String toString() {
        return "Visualizacao{" + "aluno=" + aluno + ", projeto=" + projeto + '}';
    }
    
    
}
